# Rétropsycho

Retropsycho est une application de soutien psychologique à destination des entreprises. L'application permet à des psychologues du travail de prendre en charge les diagnostiques et d'accompagner le personnel.  
Une fois par semaine les employées sont invités à soumettre leurs retours positifs et négatifs, consultables seulement par le psychologue.

## Diagramme de cas d'utilisation
---
![diagramme de cas d'utilisation](UML/use_case.png)

**Acteurs** : 
- Les employées,
- l'administateur,  

C'est deux acteurs sont internes à l'entreprise x.

- Le psychologue du travail,

externe à l'entreprise.

**Actions** :

0 : Tous les acteurs sont des utilisateurs de l'application pour y accéder, ils doivent se connecter. Ils ont aussi la possibilité de se déconnecter.

1 : L'employé envoie ses retours hedomadaires au psychologue en les rédigant et validant. L'employé peut à tout moment supprimer ses retours.

2 : L'employé peut visualiser tous ses retours. 

3 : L'administrateur a accès à la listes des participants, il peut rappeler aux non-participants à contribuer à la rétroceptive.

4 : L'administrateur gère la liste des utilisateurs; il inscrit et supprime les utilisateurs.

5 : Le psychologue du travail a accès à la liste des utilisateurs et accèder aux retours et statistiques.  

## Diagramme de séquence
---

