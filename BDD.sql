CREATE DATABASE IF NOT EXISTS psychoretro; 

CREATE USER IF NOT EXISTS 'admin'@'localhost' IDENTIFIED BY 'retro'; 

GRANT ALL PRIVILEGES ON psychoretro.* TO admin@localhost;
FLUSH PRIVILEGES;

USE psychoretro; 

CREATE TABLE IF NOT EXISTS `roles` (
    id INT AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,

    PRIMARY KEY (id)
);

ALTER TABLE roles ADD CONSTRAINT UNIQUE(name);

CREATE TABLE IF NOT EXISTS users ( 
    id INT AUTO_INCREMENT, 
    role_id INT,
    user_name VARCHAR(15) NOT NULL,
    password VARCHAR(255) NOT NULL, 
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    update_password BOOLEAN NOT NULL,
   
    PRIMARY KEY (id),
    FOREIGN KEY (role_id) REFERENCES `roles`(id)
); 

ALTER TABLE users ADD CONSTRAINT UNIQUE(user_name);


CREATE TABLE IF NOT EXISTS boards (
    id INT AUTO_INCREMENT,
    user_id INT,
    creation_date DATE DEFAULT CURRENT_DATE NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id) 
);


CREATE TABLE IF NOT EXISTS feedbacks (
    id INT AUTO_INCREMENT,
    user_id INT,
    board_id INT,
    title VARCHAR(100) NOT NULL,
    description TEXT NOT NULL,
    date_creation DATE DEFAULT CURRENT_DATE NOT NULL,
    positive BOOLEAN NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE SET NULL,
    FOREIGN KEY (board_id) REFERENCES boards(id) ON DELETE CASCADE
);

